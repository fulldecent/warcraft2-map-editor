# warcraft2-map-editor

**Note**: This project is unfinished. Terrain manipulation is not yet implemented.

<div align="center"><img src="https://gitlab.com/jcfields/warcraft2-map-editor/raw/master/screenshot.jpg" width="640" height="400" alt="[Warcraft II Map Editor]"></div>

A [map editor for *Warcraft II*](https://jcfields.gitlab.io/warcraft2-map-editor/). Written in JavaScript.

Currently implemented functionality:

- Viewing maps (unit, terrain, and movement layers).
- Placing units.
- Modifying map and player properties.
- Modifying unit, upgrade, and restriction data.
- Saving maps as a pud file or an image.

Not implemented but planned:

- Editing terrain.

Not implemented but would be nice some day:

- Undo for units and terrain.
- Copy/paste for units and terrain.
- Editing the movement map.

Works well in Firefox and Chrome. Also works in Safari, but performance is very poor.

## Acknowledgments

Based on the [pud specification](https://gitlab.com/jcfields/warcraft2-map-editor/blob/master/pudspec.txt) created by Daniel Lemberg and contributors.

Used Alexander Cech's Wardraft for extracting tilesets and tile information.

Used [ShadowFlare's GRP converter](https://sfsrealm.hopto.org/downloads/SFGrpConv.html) for converting sprites.

Uses [Feather icons](https://github.com/feathericons/feather) by [Cole Bemis](https://colebemis.com/) and contributors and [scroll icon](https://www.flaticon.com/free-icon/scroll_1009994) by [Freepik](https://www.freepik.com/).

## Authors

- J.C. Fields <jcfields+gitlab@gmail.com>

## License

- [MIT license](https://opensource.org/licenses/mit-license.php)

## External links

- [War2.ru Downloads](http://en.war2.ru/downloads/)—Maps and utilities.
